msgid ""
msgstr "Project-Id-Version: 0\nPOT-Creation-Date: 2015-10-01 18:01+0200\nPO-Revision-Date: 2021-06-28 14:32+0000\nLast-Translator: Petter Reinholdtsen <pere-weblate@hungry.com>\nLanguage-Team: Korean <https://hosted.weblate.org/projects/debian-handbook/revision_history/ko/>\nLanguage: ko-KR\nMIME-Version: 1.0\nContent-Type: application/x-publican; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=1; plural=0;\nX-Generator: Weblate 4.7.1-dev\n"

msgid "Revision History"
msgstr "수정 기록"

#, fuzzy
msgid "Raphaël"
msgstr "라파엘 (Raphaël)"

msgid "Hertzog"
msgstr "허츠고그(Hertzog)"
