<?xml version="1.0"?>
<chapter id="conclusion">
  <chapterinfo>
    <mediaobject condition="pdf">
      <imageobject>
        <imagedata fileref="images/chap-conclusion.png" scalefit="1"/>
      </imageobject>
    </mediaobject>
    <keywordset>
      <keyword>Future</keyword>
      <keyword>Improvements</keyword>
      <keyword>Opinions</keyword>
    </keywordset>
  </chapterinfo>
  <title>Conclusion: Debian's Future</title>
  <highlights>
    <para>The story of Falcot Corp ends with this last chapter; but Debian
    lives on, and the future will certainly bring many interesting
    surprises.</para>
  </highlights>
  <section id="sect.upcoming-developments">
    <title>Upcoming Developments</title>

    <!-- MAYCHANGE: Stable release number and testing codename
         https://www.debian.org/releases/testing/releasenotes -->
    <para>Now that Debian version 11 is out, the developers are already
    busy working on the next version, codenamed <emphasis
    role="distribution">Bookworm</emphasis>…</para>

    <para>There is no official list of all planned changes, and Debian never
    makes promises relating to technical goals of the coming versions.
    However, a few development trends and discussion topics can already be
    noted, and we can try to guess what might happen (or not).  <ulink
    type="block"
    url="https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html#deprecated-components"/>
    </para>

    <para>Debian will continously improve to be used in container and cloud/VPS
    systems, as well as dedicated hardware.</para>

    <para>Support for systems not using the merged-usr filesystem is at
    stake.</para>

    <indexterm><primary><command>apt-key</command></primary></indexterm>
    <para><command>apt-key</command> will become obsolete. The key management
    for third party repositories should only rely on keys found in
    <filename>/etc/apt/trusted.gpg.d</filename> or configured via
    <literal>Signed-By</literal> as described in <xref
    linkend="sect.package-authentication"/>.</para>

    <indexterm><primary><command>locate</command></primary></indexterm>
    <para>For some tasks the default software solution will change. As an
    example: <command>plocate</command> might be a faster and smaller
    replacement for <command>mlocate</command>. Of course all the main
    software suites will have a major release. The latest version of the
    various desktops will bring better usability and new features.</para>

    <para>Further the question has been raised to introduce "private" home
    directories by default by changing the default user permissions. Also using
    crypto policies and saving energy consumption can become quite big topics
    in the upcoming years.</para>

    <para>Developments which already began will continue: Improve build
    reproducibility and security for example. With the widespread use of
    continuous integration and the growth of the archive (and of the biggest
    packages!), the constraints on release architectures will be harder to meet
    and architectures will be dropped. One of the architectures at stake is
    i386. Autopkgtests (when provided) will continue to be considered across
    all architectures for migration to <emphasis
    role="distribution">Bookworm</emphasis>. In other words, the tests need to
    succeed on all release architectures for packages to migrate and improve
    stability and reliability.</para>

  </section>
  <section id="sect.future-of-debian">
    <title>Debian's Future</title>

    <para>In addition to these internal developments, one can reasonably
    expect new Debian-based distributions to come to light, as many
    tools keep simplifying this task. New specialized
    subprojects will also be started, in order to widen Debian's reach to
    new horizons.</para>

    <para>The Debian user community will increase, and new contributors
    will join the project… including, maybe, you!</para>

    <para>There are recurring discussions about how the software ecosystem
    is evolving, towards applications shipped within containers, where Debian
    packages have no added value, or with language-specific package
    managers (e.g. <command>pip</command> for Python, <command>npm</command>
    for JavaScript, etc.), which are rendering <command>dpkg</command> and
    <command>apt</command> obsolete. Facing those threats, I am convinced that
    Debian developers will find ways to embrace those evolutions and to
    continue to provide value to users.</para>

    <para>In spite of its old age and its respectable size, Debian keeps on
    growing in all kinds of (sometimes unexpected) directions. Contributors
    are teeming with ideas, and discussions on development mailing lists,
    even when they look like bickerings, keep increasing the momentum.
    Debian is sometimes compared to a black hole, of such density that any
    new free software project is attracted.</para>

    <para>Beyond the apparent satisfaction of most Debian users, a deep
    trend is becoming more and more indisputable: people are increasingly
    realizing that collaborating, rather than working alone in their corner, leads to
    better results for everyone. Such is the rationale used by
    distributions merging into Debian by way of subprojects.</para>

    <para>The Debian project is therefore not threatened by
    extinction…</para>
  </section>
  <section id="sect.future-of-this-book">
    <title>Future of this Book</title>

    <para>We would like this book to evolve in the spirit of free software.
    We therefore welcome contributions, remarks, suggestions, and
    criticism. Please direct them to Raphaël
    (<email>hertzog@debian.org</email>) or Roland
    (<email>lolando@debian.org</email>). For actionable feedback,
    feel free to open bug reports against the <literal>debian-handbook</literal>
    Debian package. The website will be used to gather
    all information relevant to its evolution, and you will find there
    information on how to contribute, in particular if you want
    to translate this book to make it available to an even
    larger public than today.
    <ulink type="block" url="https://debian-handbook.info/"/>
    <ulink type="block" url="https://bugs.debian.org/src:debian-handbook"/></para>

    <para>We tried to integrate most of what our experience with Debian
    taught us, so that anyone can use this distribution and take the best
    advantage of it as soon as possible. We hope this book contributes to
    making Debian less confusing and more popular, and we welcome publicity
    around it!</para>

    <para>We would like to conclude on a personal note. Writing (and
    translating) this book took a considerable amount of time out of our
    usual professional activity. Since we are both freelance consultants,
    any new source of income grants us the freedom to spend more time
    improving Debian; we hope this book to be successful and to contribute
    to this. In the meantime, feel free to retain our services!
    <ulink type="block" url="https://www.freexian.com"/>
    <ulink type="block" url="https://www.gnurandal.com"/>
    </para>

    <para>See you soon!</para>
  </section>
</chapter>
<!-- vim: set ft=xml tw=79 ts=2 sw=2 ai si et: -->
