#
# AUTHOR <EMAIL@ADDRESS>, YEAR.
# Ade Malsasa Akbar <teknoloid@gmail.com>, 2013. 
# Samsul Ma'arif <mail@samsul.web.id>, 2014.
msgid ""
msgstr "Project-Id-Version: 0\nPOT-Creation-Date: 2020-05-17 17:54+0200\nPO-Revision-Date: 2020-06-05 05:41+0000\nLast-Translator: Andika Triwidada <andika@gmail.com>\nLanguage-Team: Indonesian <https://hosted.weblate.org/projects/debian-handbook/03_existing-setup/id/>\nLanguage: id-ID\nMIME-Version: 1.0\nContent-Type: application/x-publican; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=1; plural=0;\nX-Generator: Weblate 4.1-dev\n"

msgid "Existing Setup"
msgstr "Penyiapan yang Ada"

msgid "Reuse"
msgstr "Penggunaan Kembali"

msgid "Migration"
msgstr "Migrasi"

msgid "Analyzing the Existing Setup and Migrating"
msgstr "Menganalisa Penyiapan yang Ada dan Memigrasikannya"

msgid "Any computer system overhaul should take the existing system into account. This allows reuse of available resources as much as possible and guarantees interoperability of the various elements comprising the system. This study will introduce a generic framework to follow in any migration of a computing infrastructure to Linux."
msgstr "Beberapa perombakan sistem komputer harus memerhatikan sistem yang ada. Hal ini mengizinkan penggunaan kembali sumberdaya yang tersedia sebanyak mungkin dan menjamin interoperabilitas dari berbagai elemen yang menyusun sistem. Studi ini akan mengenalkan kerangka umum yang diikuti dalam sebarang migrasi infrastruktur komputasi ke Linux."

msgid "Coexistence in Heterogeneous Environments"
msgstr "Koeksistensi dalam Lingkungan yang Heterogen"

msgid "<primary>environment</primary><secondary>heterogeneous environment</secondary>"
msgstr "<primary>lingkungan</primary><secondary>lingkungan heterogen</secondary>"

msgid "Debian integrates very well in all types of existing environments and plays well with any other operating system. This near-perfect harmony comes from market pressure which demands that software publishers develop programs that follow standards. Compliance with standards allows administrators to switch out programs: clients or servers, whether free or not."
msgstr "Debian berintegrasi sangat baik dalam berbagai jenis lingkungan yang ada dan berinteraksi secara baik dengan sebarang sistem operasi lain. Harmoni yang mendekati sempurna ini datang dari tekanan pasar yang menuntut bahwa penerbit perangkat lunak mengembangkan program yang mengikuti standar. Penyesuaian dengan standar mengizinkan administrator untuk berganti program: klien atau server, entah itu bebas atau tidak."

msgid "Integration with Windows Machines"
msgstr "Integrasi dengan Mesin Windows"

msgid "Samba's SMB/CIFS support ensures excellent communication within a Windows context. It shares files and print queues to Windows clients and includes software that allow a Linux machine to use resources available on Windows servers."
msgstr "Dukungan SMB/CIFS Samba memastikan komunikasi sempurna dalam konteks Windows. Itu membagikan berkas dan antrian cetak ke klien Windows dan menyertakan perangkat lunak yang mengizinkan mesin Linux untuk menggunakan sumberdaya yang ada pada server Windows."

msgid "<emphasis>TOOL</emphasis> Samba"
msgstr "<emphasis>ALAT</emphasis> Samba"

msgid "<primary>Samba</primary>"
msgstr "<primary>Samba</primary>"

msgid "The latest version of Samba can replace most of the Windows features: from those of a simple Windows NT server (authentication, files, print queues, downloading printer drivers, DFS, etc.) to the most advanced one (a domain controller compatible with Active Directory)."
msgstr "Samba versi terakhir dapat menggantikan kebanyakan fitur Windows: mulai dari milik server Windows NT sederhana (otentikasi, berkas, antrian cetak, mengunduh penggerak pencetak, DFS, dll.) sampai yang paling canggih (sebuah pengendali domain yang kompatibel dengan Active Directory)."

msgid "Integration with OS X machines"
msgstr "Integrasi dengan mesin OS X"

msgid "<primary>Zeroconf</primary>"
msgstr "<primary>Zeroconf</primary>"

msgid "<primary>Bonjour</primary>"
msgstr "<primary>Bonjour</primary>"

msgid "<primary>Avahi</primary>"
msgstr "<primary>Avahi</primary>"

msgid "OS X machines provide, and are able to use, network services such as file servers and printer sharing. These services are published on the local network, which allows other machines to discover them and make use of them without any manual configuration, using the Bonjour implementation of the Zeroconf protocol suite. Debian includes another implementation, called Avahi, which provides the same functionality."
msgstr "Mesin OS X menyediakan, dan dapat menggunakan, layanan jaringan seperti server berkas dan berbagi pencetak. Layanan-layanan ini dipublikasikan pada jaringan lokal, yang memungkinkan mesin lain untuk menemukan dan menggunakannya tanpa konfigurasi manual, menggunakan implementasi Bonjour dari keluarga protokol Zeroconf. Debian menyertakan implementasi lain, yang disebut Avahi, yang menyediakan fungsionalitas yang sama."

msgid "<primary>AFP</primary>"
msgstr "<primary>AFP</primary>"

msgid "<primary>AppleShare</primary>"
msgstr "<primary>AppleShare</primary>"

msgid "In the other direction, the Netatalk daemon can be used to provide file servers to OS X machines on the network. It implements the AFP (AppleShare) protocol as well as the required notifications so that the servers can be automatically discovered by the OS X clients."
msgstr "Pada arah yang lain, daemon Netatalk dapat digunakan untuk menyediakan server berkas untuk mesin OS X pada jaringan. Mengimplementasikan protokol AFP (AppleShare) maupun pemberitahuan yang diperlukan sehingga server dapat ditemukan secara otomatis oleh klien OS X."

msgid "<primary>AppleTalk</primary>"
msgstr "<primary>AppleTalk</primary>"

msgid "Older Mac OS networks (before OS X) used a different protocol called AppleTalk. For environments involving machines using this protocol, Netatalk also provides the AppleTalk protocol (in fact, it started as a reimplementation of that protocol). It ensures the operation of the file server and print queues, as well as time server (clock synchronization). Its router function allows interconnection with AppleTalk networks."
msgstr "Jaringan Mac OS tua (sebelum OS X) menggunakan protokol berbeda yang disebut AppleTalk. Untuk lingkungan yang melibatkan mesin menggunakan protokol ini, Netatalk juga menyediakan protokol AppleTalk (kenyataannya, dimulai dengan implementasi ulang protokol tersebut). Hal ini memastikan operasi server berkas dan antrian cetak, maupun server waktu (sinkronisasi jam). Fungsi routernya mengizinkan interkoneksi dengan jaringan AppleTalk."

msgid "Integration with Other Linux/Unix Machines"
msgstr "Integrasi dengan Mesin Linux/Unix lainnya"

msgid "Finally, NFS and NIS, both included, guarantee interaction with Unix systems. NFS ensures file server functionality, while NIS creates user directories. The BSD printing layer, used by most Unix systems, also allows sharing of print queues."
msgstr "Akhirnya, NFS dan NIS, keduanya termasuk, menjamin interaksi dengan sistem Unix. NFS memastikan fungsionalitas server berkas, sedangkan NIS membuat direktori pengguna. Lapisan pencetakan BSD, digunakan oleh kebanyak sistem Unix, juga mengizinkan berbagi antrian cetak."

msgid "Coexistence of Debian with OS X, Windows and Unix systems"
msgstr "Koeksistensi Debian dengan OS X, Windows, dan sistem Unix"

msgid "How To Migrate"
msgstr "Bagaimana Memigrasi"

msgid "<primary>migration</primary>"
msgstr "<primary>migrasi</primary>"

msgid "In order to guarantee continuity of the services, each computer migration must be planned and executed according to the plan. This principle applies whatever operating system is used."
msgstr "Untuk menjamin keberlanjutan layanan, setiap migrasi komputer harus direncanakan dan dieksekusi sesuai rencana. Prinsip ini berlaku apapun sistem operasi yang digunakan."

msgid "Survey and Identify Services"
msgstr "Survey dan Mengidentifikasi Layanan"

msgid "As simple as it seems, this step is essential. A serious administrator truly knows the principal roles of each server, but such roles can change, and sometimes experienced users may have installed “wild” services. Knowing that they exist will at least allow you to decide what to do with them, rather than delete them haphazardly."
msgstr "Walaupun terlihat sesederhana, langkah ini penting. Seorang administrator benar-benar tahu peran utama masing-masing server, namun peran dapat berubah, dan kadang pengguna berpengalaman mungkin telah memasang layanan yang “liar”. Mengetahui hal tersebut setidaknya akan mengizinkan Anda untuk memutuskan apa yang akan dilakukan dengannya, daripada menghapusnya semena-mena."

msgid "For this purpose, it is wise to inform your users of the project before migrating the server. To involve them in the project, it may be useful to install the most common free software programs on their desktops prior to migration, which they will come across again after the migration to Debian; LibreOffice and the Mozilla suite are the best examples here."
msgstr "Untuk tujuan ini, alangkah bijak untuk menginformasikan ke para pengguna proyek Anda sebelum memigrasikan server. Untuk melibatkan mereka ke dalam proyek, mungkin berguna untuk memasang program-program perangkat lunak bebas pada desktop mereka sebelum migrasi, yang akan mereka jumpai lagi setelah migrasi ke Debian; LibreOffice dan keluarga Mozilla adalah contoh terbaik di sini."

msgid "Network and Processes"
msgstr "Jaringan dan Proses"

msgid "<primary><command>nmap</command></primary>"
msgstr "<primary><command>nmap</command></primary>"

msgid "The <command>nmap</command> tool (in the package with the same name) will quickly identify Internet services hosted by a network connected machine without even requiring to log in to it. Simply call the following command on another machine connected to the same network:"
msgstr "Alat <command>nmap</command> (dalam paket dengan nama yang sama) akan mengidentifikasi secara cepat layanan Internet yang diwadahi oleh suatu mesin yang terhubung jaringan tanpa perlu masuk ke dalamnya. Panggil saja perintah berikut pada mesin lain yang terhubung pada jaringan yang sama:"

msgid ""
"\n"
"<computeroutput>$ </computeroutput><userinput>nmap mirwiz</userinput>\n"
"<computeroutput>Starting Nmap 7.40 ( https://nmap.org ) at 2017-06-06 14:41 CEST\n"
"Nmap scan report for mirwiz (192.168.1.104)\n"
"Host is up (0.00062s latency).\n"
"Not shown: 992 closed ports\n"
"PORT     STATE SERVICE\n"
"22/tcp   open  ssh\n"
"25/tcp   open  smtp\n"
"80/tcp   open  http\n"
"111/tcp  open  rpcbind\n"
"139/tcp  open  netbios-ssn\n"
"445/tcp  open  microsoft-ds\n"
"5666/tcp open  nrpe\n"
"9999/tcp open  abyss\n"
"\n"
"Nmap done: 1 IP address (1 host up) scanned in 0.06 seconds</computeroutput>"
msgstr "\n<computeroutput>$ </computeroutput><userinput>nmap mirwiz</userinput>\n<computeroutput>Starting Nmap 7.40 ( https://nmap.org ) at 2017-06-06 14:41 CEST\nNmap scan report for mirwiz (192.168.1.104)\nHost is up (0.00062s latency).\nNot shown: 992 closed ports\nPORT     STATE SERVICE\n22/tcp   open  ssh\n25/tcp   open  smtp\n80/tcp   open  http\n111/tcp  open  rpcbind\n139/tcp  open  netbios-ssn\n445/tcp  open  microsoft-ds\n5666/tcp open  nrpe\n9999/tcp open  abyss\n\nNmap done: 1 IP address (1 host up) scanned in 0.06 seconds</computeroutput>"

msgid "<emphasis>ALTERNATIVE</emphasis> Use <command>netstat</command> to find the list of available services"
msgstr "<emphasis>ALTERNATIF</emphasis> Gunakan <command>netstat</command> untuk mencari daftar layanan yang tersedia"

msgid "On a Linux machine, the <command>netstat -tupan</command> command will show the list of active or pending TCP sessions, as well UDP ports on which running programs are listening. This facilitates identification of services offered on the network."
msgstr "Pada mesin Linux, perintah <command>netstat -tupan</command> akan menampilkan daftar sesi TCP aktif maupun tertunda, juga port UDP yang ada program berjalan yang mendengarkannya. Fasilitas ini mengidentifikasi layanan yang ditawarkan oleh jaringan."

msgid "<emphasis>GOING FURTHER</emphasis> IPv6"
msgstr "<emphasis>LEBIH JAUH</emphasis> IPv6"

msgid "Some network commands may work either with IPv4 (the default usually) or with IPv6. These include the <command>nmap</command> and <command>netstat</command> commands, but also others, such as <command>route</command> or <command>ip</command>. The convention is that this behavior is enabled by the <parameter>-6</parameter> command-line option."
msgstr "Beberapa perintah jaringan mungkin bekerja dengan IPv4 (biasanya sebagai bawaan) atau IPv6. Ini termasuk perintah <command>nmap</command> dan <command>netstat</command>, tetapi juga yang lainnya, seperti <command>route</command> atau <command>ip</command>. Konvensinya adalah bahwa perilaku ini diaktifkan dengan opsi baris perintah <parameter>-6</parameter>."

msgid "If the server is a Unix machine offering shell accounts to users, it is interesting to determine if processes are executed in the background in the absence of their owner. The command <command>ps auxw</command> displays a list of all processes with their user identity. By checking this information against the output of the <command>who</command> command, which gives a list of logged in users, it is possible to identify rogue or undeclared servers or programs running in the background. Looking at <filename>crontabs</filename> (tables listing automatic actions scheduled by users) will often provide interesting information on functions fulfilled by the server (a complete explanation of <command>cron</command> is available in <xref linkend=\"sect.task-scheduling-cron-atd\" />)."
msgstr "Jika server adalah mesin Unix yang menawarkan akun shell ke pengguna, menarik untuk menentukan apakah proses dieksekusi di latar belakang tanpa kehadiran pengguna. Perintah <command>ps auxw</command> menampilkan daftar semua proses dengan identitas penggunanya. Dengan memeriksa informasi ini terhadap keluaran perintah <command>who</command>, yang menampilkan daftar pengguna yang login, memungkinkan untuk mengidentifikasi server atau program yang berjalan di latar belakang secara liar. Melihat <filename>crontabs</filename> (tabel daftar aksi otomatis terjadwal oleh pengguna) seringkali menyediakan informasi menarik pada fungsi-fungsi yangi dilayani oleh server (penjelasan lengkap tentang <command>cron</command> tersedia di <xref linkend=\"sect.task-scheduling-cron-atd\" />)."

msgid "In any case, it is essential to backup your servers: this allows recovery of information after the fact, when users will report specific problems due to the migration."
msgstr "Pada beberapa kasus, penting untuk memback-up server Anda: memungkinkan pemulihan informasi setelah sesuatu dilakukan, ketika pengguna melaporkan masalah khusus disebabkan oleh migrasi."

msgid "Backing up the Configuration"
msgstr "Mem-back up Konfigurasi"

msgid "It is wise to retain the configuration of every identified service in order to be able to install the equivalent on the updated server. The bare minimum is to make a backup copy of the configuration files."
msgstr "Bijaksana untuk memertahankan konfigurasi setiap layanan teridentifikasi agar dapat memasang yang setara pada server yang diperbarui. Setidaknya buat salinan cadangan dari berkas-berkas konfigurasi."

msgid "For Unix machines, the configuration files are usually found in <filename>/etc/</filename>, but they may be located in a sub-directory of <filename>/usr/local/</filename>. This is the case if a program has been installed from sources, rather than with a package. In some cases, one may also find them under <filename>/opt/</filename>."
msgstr "Untuk mesin Unix, berkas konfigurasi biasanya ditemukan di <filename>/etc/</filename>, tetap mungkin juga terletak di sub-direktori <filename>/usr/local/</filename>. Ini yang terjadi bila program telah dipasang dari sumber, bukan dari paket. Dalam beberapa kasus, ini mungkin juga dapat ditemukan di bawah <filename>/opt/</filename>."

msgid "For data managing services (such as databases), it is strongly recommended to export the data to a standard format that will be easily imported by the new software. Such a format is usually in text mode and documented; it may be, for example, an SQL dump for a database, or an LDIF file for an LDAP server."
msgstr "Untuk mengelola layanan data (seperti basis data), sangat direkomendasikan untuk mengekspor data ke format standar yang nantinya akan mudah diimpor oleh perangkat lunak baru. Seperti format yang biasanya berupa mode teks dan terdokumentasi; mungkin, sebagai contoh, SQL dump untuk basis data, atau berkas LDIF untuk server LDAP."

msgid "Database backups"
msgstr "Backup basisdata"

msgid "Each server software is different, and it is impossible to describe all existing cases in detail. Compare the documentation for the existing and the new software to identify the exportable (thus, re-importable) portions and those which will require manual handling. Reading this book will clarify the configuration of the main Linux server programs."
msgstr "Setiap perangkat lunak server itu berbeda, dan tidak mungkin menjelaskan semua kasus secara detail. Bandingkan dokumentasi perangkat lunak yang ada dan yang baru apakah ada bagian yang dapat dieksport (dapat diimpor) dan yang memerlukan penanganan manual. Membaca buku ini akan menglarifikasi konfigurasi program server Linux utama."

msgid "Taking Over an Existing Debian Server"
msgstr "Mengambil Alih sebuah Server Debian yang Ada"

msgid "<primary>recovering a Debian machine</primary>"
msgstr "<primary>memulihkan mesin Debian</primary>"

msgid "<primary>exploring a Debian machine</primary>"
msgstr "<primary>mengeksplorasi mesin Debian</primary>"

msgid "<primary>taking over a Debian server</primary>"
msgstr "<primary>mengambil alih server Debian</primary>"

msgid "To effectively take over its maintenance, one may analyze a machine already running with Debian."
msgstr "Agar pengambilalihan pemeliharaan efektif, pertama analisa mesin yang sedang berjalan dengan Debian."

msgid "The first file to check is <filename>/etc/debian_version</filename>, which usually contains the version number for the installed Debian system (it is part of the <emphasis>base-files</emphasis> package). If it indicates <literal><replaceable>codename</replaceable>/sid</literal>, it means that the system was updated with packages coming from one of the development distributions (either testing or unstable)."
msgstr "Berkas pertama yang dicek ialah <filename>/etc/debian_version</filename>, yang biasanya berisi nomor versi sistem Debian yang terinstall (bagian dari paket <emphasis>base-files</emphasis>). Jika menunjukkan <literal><replaceable>codename</replaceable>/sid</literal>, itu berarti sistem telah diperbarui dengan paket dari distribusi pengembangan (entah itu testing atau unstable)."

msgid "The <command>apt-show-versions</command> program (from the Debian package of the same name) checks the list of installed packages and identifies the available versions. <command>aptitude</command> can also be used for these tasks, albeit in a less systematic manner."
msgstr "Program <command>apt-show-versions</command> (dari paket Debian dengan nama sama) periksa daftar paket terinstall dan identifikasi versi yang tersedia. <command>aptitude</command> juga dapat digunakan untuk tugas ini, walaupun dengan cara yang kurang semantik."

msgid "A glance at the <filename>/etc/apt/sources.list</filename> file (and <filename>/etc/apt/sources.list.d/</filename> directory) will show where the installed Debian packages likely came from. If many unknown sources appear, the administrator may choose to completely reinstall the computer's system to ensure optimal compatibility with the software provided by Debian."
msgstr "Sekilas pada berkas <filename>/etc/apt/sources.list</filename> (dan direktori <filename>/etc/apt/sources.list.d/</filename>) akan menampilkan dari mana paket Debian terinstall. Jika ada beberapa nampak dari sumber yang tak diketahui, administrator mungkin memilih untuk menginstall ulang sepenuhnya sistem komputer untuk mengoptimalkan kompabilitas dengan perangkat lunak yang disediakan oleh Debian."

msgid "The <filename>sources.list</filename> file is often a good indicator: the majority of administrators keep, at least in comments, the list of APT sources that were previously used. But you should not forget that sources used in the past might have been deleted, and that some random packages grabbed on the Internet might have been manually installed (with the help of the <command>dpkg</command> command). In this case, the machine is misleading in its appearance of being a “standard” Debian system. This is why you should pay attention to any indication that will give away the presence of external packages (appearance of <filename>deb</filename> files in unusual directories, package version numbers with a special suffix indicating that it originated from outside the Debian project, such as <literal>ubuntu</literal> or <literal>lmde</literal>, etc.)"
msgstr "Berkas <filename>sources.list</filename> merupakan indikator yang baik: mayoritas administrator mempertahankannya, setidaknya di komentar, daftar sumber APT yang digunakan sebelumnya. Namun Anda harus ingat bahwa mungkin sumber yang digunakan sebelumnya telah dihapus, dan beberapa paket yang diambil secara acak dari Internet mungkin telah diinstall secara manual (dengan perintah <command>dpkg</command>). Dalam hal ini, mesin salah paham dalam tampilannya sebagai sistem Debian “standard”. Ini alasan mengapa Anda harus memperhatikan beberapa indikasi yang akan mengungkap kehadiran paket eksternal (berkas <filename>deb</filename> nampak di direktori yang tak biasa, nomor versi paket dengan akhiran khusus mengindikasikan paket tersebut berasal dari luar proyek Debian, seperti <literal>ubuntu</literal> atau <literal>lmde</literal>, dll.)"

msgid "Likewise, it is interesting to analyze the contents of the <filename>/usr/local/</filename> directory, whose purpose is to contain programs compiled and installed manually. Listing software installed in this manner is instructive, since this raises questions on the reasons for not using the corresponding Debian package, if such a package exists."
msgstr "Demikian pula, menarik untuk menganalisa isi dari direktori <filename>/usr/local/</filename>, yang tujuannya memang berisi program yang dicompile dan diinstall secara manual. Mendaftar perangkat lunak terinstall dengan cara ini adalah instruktif, karena ini menimbulkan pertanyaan pada alasan mengapa tidak menggunakan paket Debian yang sesuai, jika paket tersebut ada."

msgid "<emphasis>QUICK LOOK</emphasis> <emphasis role=\"pkg\">cruft</emphasis>/<emphasis role=\"pkg\">cruft-ng</emphasis>, <emphasis role=\"pkg\">debsums</emphasis> and <emphasis role=\"pkg\">apt-show-versions</emphasis>"
msgstr "<emphasis>LIHAT CEPAT</emphasis> <emphasis role=\"pkg\">cruft</emphasis>/<emphasis role=\"pkg\">cruft-ng</emphasis>, <emphasis role=\"pkg\">debsums</emphasis>, dan <emphasis role=\"pkg\">apt-show-versions</emphasis>"

msgid "The <emphasis role=\"pkg\">cruft</emphasis> and <emphasis role=\"pkg\">cruft-ng</emphasis> packages propose to list the available files that are not owned by any package. They have some filters (more or less effective, and more or less up to date) to avoid reporting some legitimate files (files generated by Debian packages, or generated configuration files not managed by <command>dpkg</command>, etc.)."
msgstr "Paket <emphasis role=\"pkg\">cruft</emphasis> dan <emphasis role=\"pkg\">cruft-ng</emphasis> mendaftar semua berkas yang tidak dimiliki oleh paket manapun. Memiliki beberapa filter (lebih atau kurang efektif, dan lebih atau kurang mutakhir) menghindari beberapa laporan legitimasi berkas (berkas yang dibangkitkan oleh paket Debian, atau berkas konfigurasi yang dibangkitkan tidak diatur oleh <command>dpkg</command>, dll.)."

msgid "Be careful to not blindly delete everything that <command>cruft</command> and <command>cruft-ng</command> might list!"
msgstr "Berhati-hatilan untuk tidak menghapus sembarangan apapun yang mungkin didaftar oleh <command>cruft</command> dan <command>cruft-ng</command>!"

msgid "The <emphasis role=\"pkg\">debsums</emphasis> package allows to check the MD5 hashsum of each file installed by a package against a reference hashsum and can help to determine, which files might have been altered (see <xref linkend=\"sidebar.debsums\" />). Be aware that created files (files generated by Debian packages, or generated configuration files not managed by <command>dpkg</command>, etc.) are not subject to this check."
msgstr "Paket <emphasis role=\"pkg\">debsums</emphasis> memungkinkan untuk memeriksa MD5 hashsum dari setiap berkas yang diinstal oleh sebuah paket terhadap sebuah referensi hashsum dan dapat membantu untuk menentukan, berkas mana yang mungkin telah diubah (lihat <xref linkend=\"sidebar.debsums\" />). Sadarilah bahwa berkas yang dibuat (berkas yang dihasilkan oleh paket Debian, atau berkas konfigurasi yang dihasilkan tidak dikelola oleh <command>dpkg</command>, dll.) tidak tunduk pada pemeriksaan ini."

msgid "The <emphasis role=\"pkg\">apt-show-versions</emphasis> package provides a tool to check for installed packages without a package source and can help to determine third party packages (see <xref linkend=\"sect.apt-show-versions\" />)."
msgstr "Paket <emphasis role=\"pkg\">apt-show-versions</emphasis> menyediakan alat untuk memeriksa paket yang diinstal tanpa sumber paket dan dapat membantu untuk menentukan paket pihak ketiga (lihat <xref linkend=\"sect.apt-show-versions\" />)."

msgid "Installing Debian"
msgstr "Menginstall Debian"

msgid "Once all the required information on the current server is known, we can shut it down and begin to install Debian on it."
msgstr "Setelah seluruh informasi yang diperlukan pada server tersebut diketahui, kita dapat mematikannya dan mulai menginstall Debian."

msgid "<primary>architecture</primary>"
msgstr "<primary>arsitektur</primary>"

msgid "To choose the appropriate version, we must know the computer's architecture. If it is a reasonably recent PC, it is most likely to be amd64 (older PCs were usually i386). In other cases, we can narrow down the possibilities according to the previously used system."
msgstr "Untuk memilih versi yang sesuai, kita harus tahu arsitektur komputernya. Jika merupakan PC terkini, sepertinya amd64 (PC lawas biasanya i386). Dalam kasus lain, kita dapat memersempit kemungkinan menurut sistem yang sebelumnya digunakan."

msgid "<xref linkend=\"tab-corresp\" xrefstyle=\"select: label nopage\" /> is not intended to be exhaustive, but may be helpful. Note that it lists Debian architectures which are no longer supported in the current stable release. In any case, the original documentation for the computer is the most reliable source to find this information."
msgstr "<xref linkend=\"tab-corresp\" xrefstyle=\"select: label nopage\" /> tidak dimaksudkan untuk lengkap, tetapi mungkin bermanfaat. Perhatikan bahwa mendaftar arsitektur Debian yang tidak lagi didukung dalam rilis stabil saat ini. Dalam hal apapun, dokumentasi asli untuk komputer adalah sumber yang paling dapat diandalkan untuk menemukan informasi ini."

msgid "Matching operating system and architecture"
msgstr "Sistem operasi dan arsitektur yang sesuai"

msgid "Operating System"
msgstr "Sistem Operasi"

msgid "Architecture(s)"
msgstr "Arsitektur"

msgid "DEC Unix (OSF/1)"
msgstr "DEC Unix (OSF/1)"

msgid "alpha, mipsel"
msgstr "alpha, mipsel"

msgid "HP Unix"
msgstr "HP Unix"

msgid "ia64, hppa"
msgstr "ia64, hppa"

msgid "IBM AIX"
msgstr "IBM AIX"

msgid "powerpc"
msgstr "powerpc"

msgid "Irix"
msgstr "Irix"

msgid "mips"
msgstr "mips"

msgid "OS X"
msgstr "OS X"

msgid "amd64, powerpc, i386"
msgstr "amd64, powerpc, i386"

msgid "z/OS, MVS"
msgstr "z/OS, MVS"

msgid "s390x, s390"
msgstr "s390x, s390"

msgid "Solaris, SunOS"
msgstr "Solaris, SunOS"

msgid "sparc, i386, m68k"
msgstr "sparc, i386, m68k"

msgid "Ultrix"
msgstr "Ultrix"

msgid "VMS"
msgstr "VMS"

msgid "alpha"
msgstr "alpha"

msgid "Windows 95/98/ME"
msgstr "Windows 95/98/ME"

msgid "i386"
msgstr "i386"

msgid "Windows NT/2000"
msgstr "Windows NT/2000"

msgid "i386, alpha, ia64, mipsel"
msgstr "i386, alpha, ia64, mipsel"

msgid "Windows XP / Windows Server 2008"
msgstr "Windows XP / Windows Server 2008"

msgid "i386, amd64, ia64"
msgstr "i386, amd64, ia64"

msgid "Windows RT"
msgstr "Windows RT"

msgid "armel, armhf, arm64"
msgstr "armel, armhf, arm64"

msgid "Windows Vista / Windows 7-8-10"
msgstr "Windows Vista / Windows 7-8-10"

msgid "i386, amd64"
msgstr "i386, amd64"

msgid "<emphasis>HARDWARE</emphasis> 64 bit PC vs 32 bit PC"
msgstr "<emphasis>PERANGKAT KERAS</emphasis> 64 bit PC vs 32 bit PC"

msgid "<primary>amd64</primary>"
msgstr "<primary>amd64</primary>"

msgid "<primary>i386</primary>"
msgstr "<primary>i386</primary>"

msgid "Most recent computers have 64 bit Intel or AMD processors, compatible with older 32 bit processors; the software compiled for “i386” architecture thus works. On the other hand, this compatibility mode does not fully exploit the capabilities of these new processors. This is why Debian provides the “amd64” architecture, which works for recent AMD chips as well as Intel “em64t” processors (including most of the Core series), which are very similar to AMD64."
msgstr "Kebanyakan komputer terkini memiliki prosesor 64 bit Intel atau AMD, kompatibel dengan prosesor 32 bit yang lebih lama; maka perangkat lunak yang di-compile untuk arsitektur “i386” bisa berjalan. Di sisi lain, kecocokan ini tidak secara penuh mengeksploitasi kemampuan prosesor baru ini. Ini alasan mengapa Debian menyediakan arsitektur “amd64”, yang bekerja untuk chip AMD terkini maupun prosesor Intel “em64t” (termasuk kebanyakan seri Core), yang sangat mirip dengan AMD64."

msgid "Installing and Configuring the Selected Services"
msgstr "Menginstall dan Mengonfigurasi Layanan Terpilih"

msgid "Once Debian is installed, we need to individually install and configure each of the services that this computer must host. The new configuration must take into consideration the prior one in order to ensure a smooth transition. All the information collected in the first two steps will be useful to successfully complete this part."
msgstr "Setelah Debian terinstall, kita harus menginstall dan mengonfigurasi satu per satu semua layanan yang komputer tersebut harus sajikan. Konfigurasi baru harus memertimbangkan yang sebelumnya untuk memastikan transisi yang mulus. Seluruh informasi yang dikumpulkan pada kedua langkah awal akan berguna untuk menyelesaikan bagian ini."

msgid "Install the selected services"
msgstr "Install layanan terpilih"

msgid "Prior to jumping into this exercise with both feet, it is strongly recommended that you read the remainder of this book. After that you will have a more precise understanding of how to configure the expected services."
msgstr "Sebelum terjun ke latihan ini dengan dua kaki, sangat disarankan Anda membaca peringatan pada buku ini. Setelah itu Anda akan lebih memahami bagaimana mengonfigurasi layanan yang diharapkan."

#~ msgid "<emphasis>QUICK LOOK</emphasis> <emphasis role=\"pkg\">cruft</emphasis>"
#~ msgstr "<emphasis>LIHAT SEKILAS</emphasis> <emphasis role=\"pkg\">cruft</emphasis>"

#~ msgid "MacOS"
#~ msgstr "MacOS"
