#
# AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2020-05-17 17:54+0200\n"
"PO-Revision-Date: 2018-02-27 11:06+0000\n"
"Last-Translator: Nicolae Crefelean <kneekoo@yahoo.com>\n"
"Language-Team: Romanian <https://hosted.weblate.org/projects/debian-handbook/book_info/ro/>\n"
"Language: ro-RO\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;\n"
"X-Generator: Weblate 2.20-dev\n"

msgid "The Debian Administrator's Handbook"
msgstr "Manualul administratorului Debian"

#, fuzzy
#| msgid "Debian Jessie from Discovery to Mastery"
msgid "Debian Buster from Discovery to Mastery"
msgstr "Debian Jessie de la descoperire la măiestrie"

msgid "Debian"
msgstr "Debian"

msgid "A reference book presenting the Debian distribution, from initial installation to configuration of services."
msgstr "O carte de referință despre distribuția Debian, de la instalarea inițială până la configurarea serviciilor."

#, fuzzy
#| msgid "ISBN: 979-10-91414-04-3 (English paperback)"
msgid "ISBN: 979-10-91414-19-7 (English paperback)"
msgstr "ISBN: 979-10-91414-04-3 (Versiunea tipărită în engleză)"

#, fuzzy
#| msgid "ISBN: 979-10-91414-05-0 (English ebook)"
msgid "ISBN: 979-10-91414-20-3 (English ebook)"
msgstr "ISBN: 979-10-91414-05-0 (Cartea în format electronic, în engleză)"

msgid "This book is available under the terms of two licenses compatible with the Debian Free Software Guidelines."
msgstr "Această carte este disponibilă sub termenii celor două licențe compatibile cu Ghidul Debian pentru Software Liber."

msgid "Creative Commons License Notice:"
msgstr "Notă de licențiere Creative Commons:"

#, fuzzy
#| msgid "This book is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License. <ulink type=\"block\" url=\"http://creativecommons.org/licenses/by-sa/3.0/\" />"
msgid "This book is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License. <ulink type=\"block\" url=\"https://creativecommons.org/licenses/by-sa/3.0/\" />"
msgstr "Această carte este licențiată sub o licență Creative Commons Atribuire - Distribuire în condiţii identice 3.0 Ne-adaptată. <ulink type=\"block\" url=\"http://creativecommons.org/licenses/by-sa/3.0/\" />"

msgid "GNU General Public License Notice:"
msgstr "Nota Licenței Publice Generale GNU:"

msgid "This book is free documentation: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version."
msgstr "Această carte este o documentație liberă: o puteți redistribui și/sau modifica conform termenilor Licenței Publice Generale GNU, așa cum este publicată de Fundația pentru Software Liber, fie versiunea 2 a licenței, fie (la opțiunea dvs.) oricare versiune ulterioară."

msgid "This book is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details."
msgstr "Această carte este distribuită cu speranța că va fi folositoare, însă FĂRĂ NICIO GARANȚIE; chiar fără garanția implicită de VANDABILITATE sau CONFORMITATE UNUI ANUMIT SCOP. Vedeți Licența Publică Generală GNU pentru mai multe detalii."

#, fuzzy
#| msgid "You should have received a copy of the GNU General Public License along with this program. If not, see <ulink url=\"http://www.gnu.org/licenses/\" />."
msgid "You should have received a copy of the GNU General Public License along with this program. If not, see <ulink url=\"https://www.gnu.org/licenses/\" />."
msgstr "Ar fi trebuit să primiți o copie a Licenței Publice Generale GNU alături de acest program. În caz contrar, vedeți <ulink url=\"http://www.gnu.org/licenses/\" />."

msgid "Show your appreciation"
msgstr "Arătați-vă aprecierea"

#, fuzzy
#| msgid "This book is published under a free license because we want everybody to benefit from it. That said maintaining it takes time and lots of effort, and we appreciate being thanked for this. If you find this book valuable, please consider contributing to its continued maintenance either by buying a paperback copy or by making a donation through the book's official website: <ulink type=\"block\" url=\"http://debian-handbook.info\" />"
msgid "This book is published under a free license because we want everybody to benefit from it. That said maintaining it takes time and lots of effort, and we appreciate being thanked for this. If you find this book valuable, please consider contributing to its continued maintenance either by buying a paperback copy or by making a donation through the book's official website: <ulink type=\"block\" url=\"https://debian-handbook.info\" />"
msgstr "Această carte este publicată sub o licență liberă pentru că vrem ca toată lumea să beneficieze de pe urma acesteia. Întreținerea acesteia necesită mult timp și efort, și apreciem mulțumirile primite. Dacă găsiți valoroasă această carte, vă rugăm considerați să contribuiți la întreținerea continuă a acesteia, fie cumpărând o copie tipărită, fie făcând o donație prin saitul oficial al cărții: <ulink type=\"block\" url=\"http://debian-handbook.info\" />"
